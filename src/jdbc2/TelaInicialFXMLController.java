/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaInicialFXMLController implements Initializable {

    @FXML
    private TableView<Pessoa> pessoasTable;
    @FXML
    private TableColumn<Pessoa, String> nomecCol;
    @FXML
    private TableColumn<Pessoa, Integer> idadeCol;
    @FXML
    private TableColumn<Pessoa, String> enderecoCol;
    @FXML
    private TableColumn<Pessoa, String> funcaoCol;
    
    @FXML
    private Button cancelarSelecaoButton;
    @FXML
    private TextField nomeField;
    @FXML
    private TextField idadeFiled;
    @FXML
    private TextField enderecoField;
    @FXML
    private Button excluirButton;
    @FXML
    private Button atualizarButton;
    @FXML
    private ToggleGroup pessoaGroup;
    @FXML
    private TextField semestreField;
    @FXML
    private Label semestreLabel;
    @FXML
    private Label cursoLabel;
    @FXML
    private TextField cursoField;
    @FXML
    private TextField setorField;
    @FXML
    private Label setorLabel;
    @FXML
    private Label funcaoLabel;
    @FXML
    private TextField funcaoField;
    @FXML
    private Label salarioLabel;
    @FXML
    private TextField salarioField;
    @FXML
    private Label disciplinaLabel;
    @FXML
    private TextField disciplinaField;
    @FXML
    private Label salarioLabel2;
    @FXML
    private TextField salarioField2;
    @FXML
    private Button cadastrarButton;
    
    private ObservableList<Pessoa> pessoasList;
    private int tipoSelecionado;
    private Pessoa pessoa;
    @FXML
    private RadioButton alunoRadio;
    @FXML
    private RadioButton funcionarioRadio;
    @FXML
    private RadioButton professorRadio;

    public int getTipoSelecionado() {
        return tipoSelecionado;
    }

    public void setTipoSelecionado(int tipoSelecionado) {
        this.tipoSelecionado = tipoSelecionado;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pessoasList = pessoasTable.getItems();
        pessoasList.clear();
        nomecCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        idadeCol.setCellValueFactory(new PropertyValueFactory<>("idade")); 
        enderecoCol.setCellValueFactory(new PropertyValueFactory<>("endereco"));
        funcaoCol.setCellValueFactory(new PropertyValueFactory<>("tipo")); 
        Aluno.getAll().forEach((a) -> {
            pessoasList.add(a);
        });
        Professor.getAll().forEach((a) -> {
            pessoasList.add(a);
        });
        FuncAdm.getAll().forEach((a) -> {
            pessoasList.add(a);
        });
        desativaTudo();
    }
    
    public void desativaTudo(){
        semestreField.setDisable(true);
        semestreLabel.setDisable(true);
        cursoField.setDisable(true);
        cursoLabel.setDisable(true);
        setorField.setDisable(true);
        setorLabel.setDisable(true);
        funcaoField.setDisable(true);
        funcaoLabel.setDisable(true);
        salarioField.setDisable(true);
        salarioLabel.setDisable(true);
        disciplinaField.setDisable(true);
        disciplinaLabel.setDisable(true);
        salarioField2.setDisable(true);
        salarioLabel2.setDisable(true);
        cancelarSelecaoButton.setDisable(true);
        excluirButton.setDisable(true);
        atualizarButton.setDisable(true);
    }

    @FXML
    private void pessoaSelecionada(MouseEvent event) {
        nomeField.setText("");
        idadeFiled.setText("");
        enderecoField.setText("");
        setorField.setText("");
        disciplinaField.setText("");
        salarioField.setText("");
        salarioField2.setText("");
        funcaoField.setText("");
        semestreField.setText("");
        cursoField.setText("");
        cadastrarButton.setDisable(true);
        alunoRadio.setDisable(true);
        professorRadio.setDisable(true);
        funcionarioRadio.setDisable(true);
        
        if(pessoasTable.getSelectionModel().getSelectedItem() instanceof Aluno){
            Aluno a = (Aluno) pessoasTable.getSelectionModel().getSelectedItem();
            nomeField.setText(a.getNome());
            idadeFiled.setText(String.valueOf(a.getIdade()));
            enderecoField.setText(a.getEndereco());
            semestreField.setText(a.getSemestre());
            cursoField.setText(a.getCurso());
            alunoRadio.setSelected(true);
            alunoSelecionado();
        }else if(pessoasTable.getSelectionModel().getSelectedItem() instanceof FuncAdm){
            FuncAdm a = (FuncAdm) pessoasTable.getSelectionModel().getSelectedItem();
            nomeField.setText(a.getNome());
            idadeFiled.setText(String.valueOf(a.getIdade()));
            enderecoField.setText(a.getEndereco());
            funcaoField.setText(a.getFuncao());
            setorField.setText(a.getSetor());
            salarioField.setText(String.valueOf(a.getSalario()));
            funcionarioSelecionado();
            funcionarioRadio.setSelected(true);
        }else if(pessoasTable.getSelectionModel().getSelectedItem() instanceof Professor){
            Professor a = (Professor) pessoasTable.getSelectionModel().getSelectedItem();
            nomeField.setText(a.getNome());
            idadeFiled.setText(String.valueOf(a.getIdade()));
            enderecoField.setText(a.getEndereco());
            disciplinaField.setText(a.getDisciplina());
            salarioField2.setText(String.valueOf(a.getSalario()));
            professorSelecionado();
            professorRadio.setSelected(true);
        }
        cancelarSelecaoButton.setDisable(false);
        excluirButton.setDisable(false);
        atualizarButton.setDisable(false);
        
    }

    

    @FXML
    private void cancelarSelecao() {
        desativaTudo();
        pessoasTable.getSelectionModel().clearSelection();
        nomeField.setText("");
        idadeFiled.setText("");
        enderecoField.setText("");
        setorField.setText("");
        disciplinaField.setText("");
        salarioField.setText("");
        salarioField2.setText("");
        funcaoField.setText("");
        semestreField.setText("");
        cursoField.setText("");
        cadastrarButton.setDisable(false);
        alunoRadio.setDisable(false);
        professorRadio.setDisable(false);
        funcionarioRadio.setDisable(false);
        alunoRadio.setSelected(false);
        professorRadio.setSelected(false);
        funcionarioRadio.setSelected(false);
        
    }

    @FXML
    private void excluirDados(ActionEvent event) {
        
        pessoasTable.getSelectionModel().getSelectedItem().delete();
        pessoasList.remove(pessoasTable.getSelectionModel().getSelectedItem());
        
        cancelarSelecao();
    }

    @FXML
    private void atualizarDados(ActionEvent event) {
        if(pessoasTable.getSelectionModel().getSelectedItem() instanceof Aluno){
            Aluno pessoa = (Aluno) pessoasTable.getSelectionModel().getSelectedItem();
            pessoa.setCurso(cursoField.getText());
            pessoa.setEndereco(enderecoField.getText());
            pessoa.setIdade(Integer.valueOf(idadeFiled.getText()));
            pessoa.setNome(nomeField.getText());
            pessoa.setSemestre(semestreField.getText());
            pessoa.update();
            alunoRadio.setSelected(false);
        }else if(pessoasTable.getSelectionModel().getSelectedItem() instanceof FuncAdm){
            FuncAdm pessoa = (FuncAdm) pessoasTable.getSelectionModel().getSelectedItem();
            pessoa.setFuncao(funcaoField.getText());
            pessoa.setEndereco(enderecoField.getText());
            pessoa.setIdade(Integer.valueOf(idadeFiled.getText()));
            pessoa.setNome(nomeField.getText());
            pessoa.setSalario(Double.valueOf(salarioField.getText()));
            pessoa.setSetor(setorField.getText());
            pessoa.update();
            funcionarioRadio.setSelected(false);
        }else if(pessoasTable.getSelectionModel().getSelectedItem() instanceof Professor){
            Professor pessoa = (Professor) pessoasTable.getSelectionModel().getSelectedItem();
            pessoa.setDisciplina(disciplinaField.getText());
            pessoa.setEndereco(enderecoField.getText());
            pessoa.setIdade(Integer.valueOf(idadeFiled.getText()));
            pessoa.setNome(nomeField.getText());
            pessoa.setSalario(Double.valueOf(salarioField2.getText()));
            pessoa.update();
            professorRadio.setSelected(false);
        }
        pessoasTable.refresh();
        cancelarSelecao();
    }

    @FXML
    private void alunoSelecionado() {
        desativaTudo();
        semestreField.setDisable(false);
        semestreLabel.setDisable(false);
        cursoField.setDisable(false);
        cursoLabel.setDisable(false);
        setTipoSelecionado(1);
    }

    @FXML
    private void funcionarioSelecionado() {
        desativaTudo();
        setorField.setDisable(false);
        setorLabel.setDisable(false);
        funcaoField.setDisable(false);
        funcaoLabel.setDisable(false);
        salarioField.setDisable(false);
        salarioLabel.setDisable(false);
        setTipoSelecionado(2);
        
    }

    @FXML
    private void professorSelecionado() {
        desativaTudo();
        disciplinaField.setDisable(false);
        disciplinaLabel.setDisable(false);
        salarioField2.setDisable(false);
        salarioLabel2.setDisable(false);
        setTipoSelecionado(3);
    }

    @FXML
    private void cadastrarDados(ActionEvent event) {
        if(getTipoSelecionado() == 1){
            Aluno pessoa = new Aluno();
            pessoa.setCurso(cursoField.getText());
            pessoa.setEndereco(enderecoField.getText());
            pessoa.setIdade(Integer.valueOf(idadeFiled.getText()));
            pessoa.setNome(nomeField.getText());
            pessoa.setSemestre(semestreField.getText());
            pessoa.insert();
            pessoasList.add(pessoa);
        }else if(getTipoSelecionado() == 2){
            FuncAdm pessoa = new FuncAdm();
            pessoa.setFuncao(funcaoField.getText());
            pessoa.setEndereco(enderecoField.getText());
            pessoa.setIdade(Integer.valueOf(idadeFiled.getText()));
            pessoa.setNome(nomeField.getText());
            pessoa.setSalario(Double.valueOf(salarioField.getText()));
            pessoa.setSetor(setorField.getText());
            pessoa.insert();
            pessoasList.add(pessoa);
        }else if(getTipoSelecionado() == 3){
            Professor pessoa = new Professor();
            pessoa.setDisciplina(disciplinaField.getText());
            pessoa.setEndereco(enderecoField.getText());
            pessoa.setIdade(Integer.valueOf(idadeFiled.getText()));
            pessoa.setNome(nomeField.getText());
            pessoa.setSalario(Double.valueOf(salarioField2.getText()));
            pessoa.insert();
            pessoasList.add(pessoa);
        }
        cancelarSelecao();
        
    }
    
}
