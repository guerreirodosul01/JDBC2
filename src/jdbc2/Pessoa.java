/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public abstract class Pessoa {

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    private String nome,endereco,tipo;
    private int idade,id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null,ps2=null;

        String insertTableSQL = "DELETE FROM OO_PESSOAGENERICAJDBC2 WHERE ID = ?";        
        
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, getId());
            
            if(getTipo().equals("PROFESSOR")){
                String insertTableSQL2 = "DELETE FROM \"BD3_INT08\".\"OO_ProfessorJDBC2\" WHERE ID = ?";
                ps2 = dbConnection.prepareStatement(insertTableSQL2);
                ps2.setInt(1, getId());
                ps2.executeUpdate();
            }else if(getTipo().equals("FUNCADM")){
                String insertTableSQL2 = "DELETE FROM \"BD3_INT08\".\"OO_FuncJDBC2\" WHERE ID = ?";
                ps2 = dbConnection.prepareStatement(insertTableSQL2);
                ps2.setInt(1, getId());
                ps2.executeUpdate();
                
            }else if(getTipo().equals("ALUNO")){
                String insertTableSQL2 = "DELETE FROM \"BD3_INT08\".\"OO_AlunoJDBC2\" WHERE ID = ?";
                ps2 = dbConnection.prepareStatement(insertTableSQL2);
                ps2.setInt(1, getId());
                ps2.executeUpdate();
                
            }
          
            ps.executeUpdate();
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }      
        
        return true;
        
    }

    public Pessoa() {
        this.id = (int) (Math.random() * 10000);
    }
    
}
