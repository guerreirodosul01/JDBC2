/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class FuncAdm extends Funcionario{
    
    private String setor,funcao;

    public FuncAdm() {
        setTipo("FUNCADM");
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null,preparedStatement2 = null;

        String insertTableSQL = "INSERT INTO OO_PESSOAGENERICAJDBC2 (NOME, IDADE , ENDERECO,TIPO,ID ) VALUES (?,?,?,?,?)";
        String insertTableSQL2 = "INSERT INTO \"BD3_INT08\".\"OO_FuncJDBC2\" (SETOR, FUNCAO,SALARIO,ID ) VALUES (?,?,?,?)";

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement2 = dbConnection.prepareStatement(insertTableSQL2);
            preparedStatement.setString(1, getNome());
            preparedStatement.setInt(2, getIdade());
            preparedStatement.setString(3,getEndereco());
            preparedStatement2.setString(1, getSetor());
            preparedStatement2.setString(2, getFuncao());
            preparedStatement2.setDouble(3, getSalario());
            preparedStatement.setString(4, "FUNCADM");
            preparedStatement.setInt(5, getId());
            preparedStatement2.setInt(4, getId());
            
            preparedStatement2.executeUpdate();
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    
    
    public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE OO_PESSOAGENERICAJDBC2 SET NOME =?, IDADE= ?, ENDERECO=?,TIPO =? WHERE ID = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, getNome());
            ps.setInt(2, getIdade());
            ps.setString(3, getEndereco());
            ps.setString(4, getTipo());
            ps.setInt(5, getId());
          
            update2();
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public boolean update2() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE \"BD3_INT08\".\"OO_FuncJDBC2\" SET SETOR = ?,FUNCAO = ? ,SALARIO=? WHERE ID = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, getSetor());
            ps.setString(2, getFuncao());
            ps.setDouble(3, getSalario());
            ps.setInt(4, getId());
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public static ArrayList<FuncAdm> getAll() {
        String selectSQL = "SELECT * FROM OO_PESSOAGENERICAJDBC2 WHERE TIPO = 'FUNCADM'";
        ArrayList<FuncAdm> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        PreparedStatement st, st2 = null;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                FuncAdm f = new FuncAdm();
                f.setNome(rs.getString("NOME"));
                f.setIdade(rs.getInt("IDADE"));
                f.setEndereco(rs.getString("ENDERECO"));
                f.setTipo("FUNCADM");
                f.setId(rs.getInt("ID"));
                
                String selectSQL2 = "SELECT * FROM \"BD3_INT08\".\"OO_FuncJDBC2\" WHERE ID = ?";
                st2 = dbConnection.prepareStatement(selectSQL2);
                st2.setInt(1, rs.getInt("ID"));
                ResultSet rs2 = st2.executeQuery();
                if (rs2.next()) {
                    f.setSetor(rs2.getString("SETOR"));
                    f.setFuncao(rs2.getString("FUNCAO"));
                    f.setSalario(rs2.getDouble("SALARIO"));
                }
                
                
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }
 }
    

