/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Aluno extends Pessoa{
    
    private String semestre,curso;

    public Aluno() {
        setTipo("ALUNO");
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement =null ,preparedStatement2 = null;

        String insertTableSQL = "INSERT INTO OO_PESSOAGENERICAJDBC2 (NOME, IDADE , ENDERECO ,TIPO, ID ) VALUES (?,?,?,?,?)";
        String insertTableSQL2 = "INSERT INTO \"BD3_INT08\".\"OO_AlunoJDBC2\" (SEMESTRE, CURSO,ID ) VALUES (?,?,?)";

        try {
            preparedStatement2 = dbConnection.prepareStatement(insertTableSQL2);
            preparedStatement2.setString(1, getSemestre());
            preparedStatement2.setString(2, getCurso());
            preparedStatement2.setInt(3, getId());
            
            preparedStatement2.executeUpdate();
            
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, getNome());
            preparedStatement.setInt(2, getIdade());
            preparedStatement.setString(3,getEndereco());
            preparedStatement.setString(4, "ALUNO");
            preparedStatement.setInt(5, getId());
            
            preparedStatement.executeUpdate();
            
            
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        } 
        
        return true;
    }
    
    
    
    
    
    public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE OO_PESSOAGENERICAJDBC2 SET NOME =?, IDADE= ?, ENDERECO=?  WHERE ID = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, getNome());
            ps.setInt(2, getIdade());
            ps.setString(3, getEndereco());
            ps.setInt(4, getId());
          
            update2();
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public boolean update2() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE \"BD3_INT08\".\"OO_AlunoJDBC2\" SET SEMESTRE = ?,CURSO = ? WHERE ID = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, getSemestre());
            ps.setString(2, getCurso());
            ps.setInt(3, getId());
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public static ArrayList<Aluno> getAll() {
        String selectSQL = "SELECT * FROM OO_PESSOAGENERICAJDBC2 WHERE TIPO = 'ALUNO'";
        
        
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st,st2 = null;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = st.executeQuery();
            
            
            while (rs.next()) {
                

                Aluno f = new Aluno();
                f.setNome(rs.getString("NOME"));
                f.setIdade(rs.getInt("IDADE"));
                f.setEndereco(rs.getString("ENDERECO"));
                f.setTipo("ALUNO");
                f.setId(rs.getInt("ID"));
                
                String selectSQL2 = "SELECT * FROM \"BD3_INT08\".\"OO_AlunoJDBC2\" WHERE ID = ?";
                st2 = dbConnection.prepareStatement(selectSQL2);
                st2.setInt(1, rs.getInt("ID"));
                ResultSet rs2 = st2.executeQuery();
                if (rs2.next()) {
                    f.setSemestre(rs2.getString("SEMESTRE"));
                    f.setCurso(rs2.getString("CURSO"));
                }
                
                
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }
    
    
    
    
}
